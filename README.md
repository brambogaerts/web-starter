# Web Starter

Starter project for my preferred web stack.

## 🤷‍ Why?

Primarily for internal use, as I like to have some control over my stack and project structure.

## 🧰 What's the stack?

- [NVM](https://github.com/nvm-sh/nvm) for, well... Node Version Management 😎.
- [Node.js](https://github.com/nodejs/node) for server-side scripting. As I come from PHP, Node was a breath of fresh air, and I've never looked back.
- [Express](https://github.com/expressjs/express) as a Node web server. Does its job, lovely API.
- [PostgreSQL](https://www.postgresql.org) as the database layer. I used MongoDB for a long time and recently switched to PostgreSQL, preferring SQL over NoSQL.
- [Sass](https://github.com/sass/sass) allows us to create beautiful things in an organized fashion 💅.
- [React](https://github.com/facebook/react) - we've tried them all and even developed our own, but React definitely is the best for our purposes 👑.
- [Redux](https://github.com/reduxjs/redux) makes it SO EASY to manage state across our app.
- [Babel](https://github.com/babel/babel) - in the end I love to use the latest and greatest ECMAScript features but want cross-compatible apps. Babel allows me both.
- [Webpack](https://github.com/webpack/webpack) is such a monster to set up correctly - but once it's up and running its power is immense. Probably the main reason for creating this starter template.
- [ESLint](https://github.com/eslint/eslint) because it helps me catch errors before I even make them ⚡️.
- [Prettier](https://github.com/prettier/prettier) because I absolutely NEED my code formatted and linted just the way I like - AirBnB style with single quotes and tabs. I've set ESLint up with it to lint on save.

## 🚀 Usage

Run `npm install`, and then either `npm start` to develop or `npm run build` to build for production.
