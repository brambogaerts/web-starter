import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import reduce from '../lib/reducers';
import defaultState from '../lib/defaultState';

const store = createStore(reduce, defaultState);

const App = () => (
	<Provider store={store}>
		<div>Hello world!</div>
	</Provider>
);

export default hot(App);
