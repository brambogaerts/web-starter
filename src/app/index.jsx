import 'reset-scss';

import React from 'react';
import ReactDOM from 'react-dom';

import './sass/main.scss';
import App from './components/App';

const container = document.createElement('div');
container.classList.add('app');
document.body.appendChild(container);

ReactDOM.render(<App />, container);
