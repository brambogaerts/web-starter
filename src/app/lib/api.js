export async function getItems() {
	const response = await fetch('/api/v1/items');
	return await response.json();
}
