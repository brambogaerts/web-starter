module.exports = {
	title: '', // Title given to the HTML page
	db: {
		user: '', // PostgreSQL username
		host: '', // PostgreSQL host -- usually localhost
		database: '', // PostgreSQL database name
		password: '', // PostgreSQL password
		port: 5432 // PostgreSQL port
	}
};
