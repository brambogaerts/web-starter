const express = require('express');

function createRoutes(client) {
	const router = new express.Router();

	router.get('/api/v1/items/', (req, res) => {
		client
			.query(`SELECT * FROM items`)
			.then(results => {
				res.send(results.rows);
			})
			.catch(error => {
				res.status(400).send(error);
			});
	});

	return router;
}

module.exports = createRoutes;
