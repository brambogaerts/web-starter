const express = require('express');
const http = require('http');
const pg = require('pg');
const config = require('../config');

// Create an app instance
const app = express();
const server = http.Server(app);

// Connect to the database
const client = new pg.Client(config.db);
client.connect();

// Read the PORT environment variable or default to port 9081
app.set('port', process.env.PORT || 8080);

// Use static directory
app.use(express.static(`${__dirname}/../../build`));

// Import controllers

app.use(require('./controllers/items')(client));

// Start the app
server.listen(app.get('port'));
