const path = require('path');
const webpack = require('webpack');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const config = require('./src/config/');

module.exports = {
	context: path.resolve(__dirname, 'src/app'),
	entry: ['@babel/polyfill', './index.jsx'],
	output: {
		path: path.resolve('./build'),
		filename: '[name].bundle.[hash].js',
		publicPath: '/'
	},
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			'react-dom': '@hot-loader/react-dom'
		}
	},
	module: {
		rules: [
			// JavaScript
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env', '@babel/preset-react'],
							plugins: ['react-hot-loader/babel']
						}
					}
				]
			},
			// Sass
			{
				test: /\.(sass|scss)$/,
				use: ['style-loader', 'css-loader', 'sass-loader']
			},
			// Fonts, images and movies
			{
				test: /\.(woff|png|mp4)$/,
				loader: 'url-loader?limit=65000'
			},
			// Graphics
			{
				test: /\.svg$/,
				loader: 'raw-loader'
			}
		]
	},
	plugins: [
		new FaviconsWebpackPlugin({
			logo: path.resolve(__dirname, 'src/app/img/logo.png'),
			prefix: 'icons-[hash]/',
			inject: true
		}),
		new HTMLWebpackPlugin({
			title: config.title
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	devtool: 'eval',
	devServer: {
		port: process.env.DEVPORT || 3000,
		proxy: {
			'*': `http://0.0.0.0:${process.env.PORT || 8080}`
		},
		hot: true
	}
};
